const express = require ("express")
const mongoose = require ("mongoose")
const dotenv = require ("dotenv")

dotenv.config()

const app = express()
const port = 3005


// MongoDB Connection
mongoose.connect(`mongodb+srv://kristel_maicong:${process.env.MONGODB_PASSWORD}@cluster0.8n8kqom.mongodb.net/?retryWrites=true&w=majority`, {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

let db = mongoose.connection
db.on('error', () => console.error("Connection Error"))
db.on('open', () => console.error("Connected to MongoDB!"))
// MongoDB Connection End


app.use(express.json())
app.use(express.urlencoded({extended: true}))

// MongoDB Schemas 
const userSchema = new mongoose.Schema({
 username: String,
  password: {
    type: String,
    default: ''
  }
})
// MongoDB Schemas END


// MongoDB Model 
const User = mongoose.model('User',userSchema) 

// MongoDB Model END 


// Routes 
  

app.post('/signup', (request, response) => {
  User.findOne({username: request.body.username},(error, result) => {
    if(result != null && result.username == request.body.username){ 
      return response.send('Duplicate user Registered!')
    }
    let newUser = new User({
      username: request.body.username, 
      password: request.body.password
    }) 


    newUser.save((error, savedTask) => {
       if(error){
        return console.error(error)
       }
       else {
        return response.status(200).send('New user registered!')
       }
    })
  })
})

app.get('/signup',(request,response) =>{
  User.find({}, (error, result) => {
      if (error){
        return console.log(error)
      }
       return response.status(200).json({
        data:result
      })
   })
})

// Routes End 


app.listen(port, () => console.log(`Server running at local host:${port}`))